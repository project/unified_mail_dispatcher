<?php

namespace Drupal\unified_mail_dispatcher;

use Drupal\Core\Mail\MailManager;

/**
 * Class UnifiedMailManager.
 *
 * {@inheritdoc}
 */
class UnifiedMailManager extends MailManager {

  /**
   * {@inheritdoc}
   */
  public function mail($module, $key, $to, $langcode, $params = [], $reply = NULL, $send = TRUE) {
    $config = \Drupal::config('unified_mail_dispatcher.settings');
    $alter  = $config->get('mail_alter');

    if ($alter) {
      $option = $config->get('alter_option');
      if ($option == 'discord') {
        // Load the discord service.
        $discord_manager = \Drupal::service('unified_mail_dispatcher.discord_manager');
        $webhook_url     = $config->get('webhook_url');

        // Construct your message.
        $message = [
          'content' => "Module: $module\nKey: $key\nRecipient: $to\nLanguage: $langcode\nParams: ",
        ];
        $discord_manager->send($webhook_url, $message);

        return TRUE;
      }
      elseif ($option == 'email') {
        // Send email to the specified single email.
        $to = $config->get('email');
      }
    }
    // If not altered, or email option is selected, send the email normally.
    return parent::mail($module, $key, $to, $langcode, $params, $reply, $send);
  }

}
