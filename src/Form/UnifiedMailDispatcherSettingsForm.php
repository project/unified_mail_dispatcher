<?php

namespace Drupal\unified_mail_dispatcher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class UnifiedMailDispatcherSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unified_mail_dispatcher_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unified_mail_dispatcher.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unified_mail_dispatcher.settings');

    $form['mail_alter'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Activate Route Alter'),
      '#default_value' => $config->get('mail_alter'),
    ];

    $form['alter_option'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Alter Option'),
      '#options'       => [
        'discord' => $this->t('Send to Discord'),
        'email'   => $this->t('Send to Single Email'),
      ],
      '#default_value' => $config->get('alter_option'),
      '#states'        => [
        'visible' => [
          ':input[name="mail_alter"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['webhook_url'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Discord Webhook URL'),
      '#default_value' => $config->get('webhook_url'),
      '#states'        => [
        'visible' => [
          ':input[name="alter_option"]' => ['value' => 'discord'],
          ':input[name="mail_alter"]'   => ['checked' => TRUE],
        ],
      ],
    ];

    $form['email'] = [
      '#type'          => 'email',
      '#title'         => $this->t('Single Email Address'),
      '#default_value' => $config->get('email'),
      '#states'        => [
        'visible' => [
          ':input[name="alter_option"]' => ['value' => 'email'],
          ':input[name="mail_alter"]'   => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unified_mail_dispatcher.settings')
      ->set('mail_alter', $form_state->getValue('mail_alter'))
      ->set('alter_option', $form_state->getValue('alter_option'))
      ->set('webhook_url', $form_state->getValue('webhook_url'))
      ->set('email', $form_state->getValue('email'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
