<?php

namespace Drupal\unified_mail_dispatcher;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Class DiscordManager.
 *
 * {@inheritdoc}
 */
class DiscordManager {

  use StringTranslationTrait;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new DiscordManager object.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->logger     = $logger_factory->get('unified_mail_dispatcher');
  }

  /**
   * Sends a message to the Discord channel via the provided webhook URL.
   *
   * @param string $webhook_url
   *   The Discord webhook URL.
   * @param array $message
   *   The message to be sent.
   *
   * @return bool
   *   Returns TRUE on success, FALSE on failure.
   */
  public function send(string $webhook_url, array $message): bool {
    try {
      $response = $this->httpClient->request('POST', $webhook_url, [
        'json' => $message,
      ]);

      // Check if the request was successful.
      if ($response->getStatusCode() == 204) {
        return TRUE;
      }
    }
    catch (RequestException $e) {
      $this->logger->error($this->t('Could not send Discord notification. Error message: @error', ['@error' => $e->getMessage()]));
    }

    return FALSE;
  }

}
