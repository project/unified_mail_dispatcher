The Unified Mail Dispatcher module alters the Drupal mail system to either send notifications to a specified Discord channel via a webhook or to a single email address.

Installation
Download and place the module folder in your modules directory (usually web/modules/custom/).
Enable the module either through the Drupal administration interface or by using Drush: drush en unified_mail_dispatcher.
Configure the Discord webhook URL or the single email address at /admin/config/system/unified-mail-dispatcher.
Configuration
To generate a Discord webhook, open Discord and navigate to the channel where you want the notifications to be posted.

Click on the gear icon next to the channel name to open its settings. Go to the "Integrations" tab and create a new webhook. Copy the webhook URL.
To specify a single email address, simply enter the email address in the configuration form.
You can switch between Discord notifications and email by selecting the appropriate option in the configuration form.
Note: The module will not work unless a valid Discord webhook URL or email address is specified in the configuration.
